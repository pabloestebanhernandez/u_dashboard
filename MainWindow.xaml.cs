﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Threading;
using System.IO.Ports;

namespace WpfChart3._5
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Brush green = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#388E3C"));
        Brush gris = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#757575"));
        public delegate void changeGraph();
        private bool continueOperating = false;
        private DateTime startTime;
        private double count = 1;
        ObservableCollection<KeyValuePair<double, double>> Power = new ObservableCollection<KeyValuePair<double, double>>();

        private SerialPort comPort = new SerialPort();

        public MainWindow()
        {
            InitializeComponent();
        }

        public void readTemperature()
        {
            if (DateTime.Compare(DateTime.Now, startTime.AddSeconds(Double.Parse("3"))) >= 0)
            {

                double temp = 0;
                double light_1 = 0;
                double light_2 = 0;
                double light_3 = 0;
                double light_4 = 0;

                try
                {
                    string data = comPort.ReadExisting();

                    data = data.Substring(0, data.IndexOf(';') - 1);

                    temp = Convert.ToDouble(data.Substring(0, data.IndexOf('|')));
                    data = data.Substring(data.IndexOf('|') + 1);
                    light_1 = Convert.ToDouble(data.Substring(0, data.IndexOf('|')));
                    data = data.Substring(data.IndexOf('|') + 1);
                    light_2 = Convert.ToDouble(data.Substring(0, data.IndexOf('|')));
                    data = data.Substring(data.IndexOf('|') + 1);
                    light_3 = Convert.ToDouble(data.Substring(0, data.IndexOf('|')));
                    data = data.Substring(data.IndexOf('|') + 1);
                    light_4 = Convert.ToDouble(data);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }

                if (count >= 30)
                {
                    Power.Clear();
                    count = 1;
                }

                startTime = DateTime.Now;
                tempSlider.Value = temp;
                //tempValue = double.Parse(txtTempValue.Text);
                Power.Add(new KeyValuePair<double, double>(count, temp));
                waitingtasks.DataContext = Power;
                slider1.Value = light_1;
                slider_Copy.Value = light_2;
                slider_Copy1.Value = light_3;
                slider_Copy2.Value = light_4;
                logTemp.Text += DateTime.Now.ToString() + " - " + temp.ToString() + "\n";
                count += 1;

            }
            if (continueOperating)
            {
                startStop.Dispatcher.BeginInvoke(DispatcherPriority.ApplicationIdle, new changeGraph(readTemperature));
            }
        }

        private void chkAutoScoll_Checked(object sender, RoutedEventArgs e)
        {
            logTemp.AutoScroll = (bool)chkAutoScroll.IsChecked;
        }

        private void startStop_Click(object sender, RoutedEventArgs e)
        {
            if (continueOperating)
            {
                continueOperating = false;
                startStop.Content = "Resume";
                comPort.Close();
            }
            else
            {
                comPort.PortName = "COM4";
                comPort.BaudRate = Convert.ToInt16(9600);
                comPort.DtrEnable = true;
                comPort.RtsEnable = true;
                comPort.Open();
                continueOperating = true;
                if (startStop.Content.ToString() != "Resume")
                {
                    comPort.Write("1");
                }
                startStop.Content = "Stop";
                startTime = DateTime.Now;
                startStop.Dispatcher.BeginInvoke(DispatcherPriority.Normal, new changeGraph(readTemperature));
            }
        }

        private void chkAutoScroll_Unchecked(object sender, RoutedEventArgs e)
        {
            logTemp.AutoScroll = (bool)chkAutoScroll.IsChecked;
        }

        private void luz1_Click(object sender, RoutedEventArgs e)
        {
            Button luz = (Button)sender;

            if (luz.Content.Equals("OFF"))
            {
                luz.Content = "ON";
                slider1.Value = 100;
                luz.Background = green;
            }
            else
            {
                luz.Content = "OFF";
                slider1.Value = 0;
                luz.Background = gris;
            }



        }


        private void luz2_Click(object sender, RoutedEventArgs e)
        {
            Button luz = (Button)sender;

            if (luz.Content.Equals("OFF"))
            {
                luz.Content = "ON";
                slider_Copy.Value = 100;
                luz.Background = green;
            }
            else
            {
                luz.Content = "OFF";
                slider_Copy.Value = 0;
                luz.Background = gris;
            }
        }

        private void luz3_Click(object sender, RoutedEventArgs e)
        {
            Button luz = (Button)sender;

            if (luz.Content.Equals("OFF"))
            {
                luz.Content = "ON";
                slider_Copy1.Value = 100;
                luz.Background = green;
            }
            else
            {
                luz.Content = "OFF";
                slider_Copy1.Value = 0;
                luz.Background = gris;
            }
        }

        private void luz4_Click(object sender, RoutedEventArgs e)
        {
            Button luz = (Button)sender;

            if (luz.Content.Equals("OFF"))
            {
                luz.Content = "ON";
                slider_Copy2.Value = 100;
                luz.Background = green;
            }
            else
            {
                luz.Content = "OFF";
                slider_Copy2.Value = 0;
                luz.Background = gris;
            }
        }

        private void tempSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {

        }

        private void txtluz1_TextChanged(object sender, TextChangedEventArgs e)

        {
            TextBox intensidad_luz = (TextBox)sender;
            int intensidad = int.Parse(intensidad_luz.Text);

            if (intensidad == 0)
                
            {
                luz1.Content = "OFF";
                luz1.Background = gris;
            }
            else if(intensidad>0)
            {
                luz1.Content = "ON";
                luz1.Background = green;

            }

        }

        private void txtluzCopy_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox intensidad_luz = (TextBox)sender;
            int intensidad;

            intensidad = int.Parse(intensidad_luz.Text);

            if (intensidad == 0)
            {
                luz2.Content = "OFF";
                luz2.Background = gris;
            }
            else if (intensidad>0)
            {
                luz2.Content = "ON";
                luz2.Background = green;

            }

        }

        private void txtluzCopy1_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox intensidad_luz = (TextBox)sender;
            int intensidad = int.Parse(intensidad_luz.Text);

            if (intensidad == 0)
               
            {
                luz3.Content = "OFF";
                luz3.Background = gris;
            }
            else if (intensidad>0)
            {
                luz3.Content = "ON";
                luz3.Background = green;

            }

        }

        private void txtluzCopy2_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox intensidad_luz = (TextBox)sender;
           int  intensidad = int.Parse(intensidad_luz.Text);

            if (intensidad == 0)
                
            {
                luz4.Content = "OFF";
                luz4.Background = gris;
            }
            else if (intensidad>0)
            {
                luz4.Content = "ON";
                luz4.Background = green;

            }

        }




    }

    public class ScrollingTextBox : TextBox
    {
        private bool autoScroll = false;

        public bool AutoScroll
        {
            get
            {
                return autoScroll;
            }

            set
            {
                autoScroll = value;
            }
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);
            VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
            HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
        }

        protected override void OnTextChanged(TextChangedEventArgs e)
        {
            if (autoScroll)
            {
                base.OnTextChanged(e);
                CaretIndex = Text.Length;
                ScrollToEnd();
            }
        }

    }
}
